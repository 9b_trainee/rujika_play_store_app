package com.internship.tga.modelnew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @Expose
    @SerializedName("AppsData")
    private List<AppsData> AppsData;

    public List<AppsData> getAppsData() {
        return AppsData;
    }

    public void setAppsData(List<AppsData> AppsData) {
        this.AppsData = AppsData;
    }
}
