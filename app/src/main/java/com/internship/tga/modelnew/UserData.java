package com.internship.tga.modelnew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {
    @Expose
    @SerializedName("IsFreeUser")
    private String IsFreeUser;
    @Expose
    @SerializedName("WebsiteURL")
    private String WebsiteURL;
    @Expose
    @SerializedName("Country")
    private String Country;
    @Expose
    @SerializedName("URL")
    private String URL;
    @Expose
    @SerializedName("CompanyName")
    private String CompanyName;

    public String getIsFreeUser() {
        return IsFreeUser;
    }

    public void setIsFreeUser(String IsFreeUser) {
        this.IsFreeUser = IsFreeUser;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String WebsiteURL) {
        this.WebsiteURL = WebsiteURL;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }
}
