package com.internship.tga.modelnew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuccessData {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Status")
    private String Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
}
