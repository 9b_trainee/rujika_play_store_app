package com.internship.tga;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.internship.tga.Constant.BaseUrl;
import com.internship.tga.Interface.ApiInterface;
import com.internship.tga.Model.AppRatingsClass;
import com.internship.tga.Model.AppsData;
import com.internship.tga.Model.DataClass;
import com.internship.tga.Model.GetDetails;
import com.internship.tga.Model.UserDataClass;
import com.internship.tga.modelnew.SuccessData;

import junit.framework.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TGAStore extends AppCompatActivity {

    GridView grid;
    String status = "Success";
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Tests> testList = new ArrayList<>();
    ArrayList<DataClass> datalist = new ArrayList<DataClass>();
    private String appIcon;
    private String appName;
    private String avgRatings;
    private String userVotes;
    private String androidUrl;
    private MyAdapter myAdapter;
    //CustomGrid adapter = new CustomGrid(TGAStore.this, appIcon , appName , avgRatings , userVotes);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tgastore);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAdapter = new MyAdapter(getApplicationContext(), testList);
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        //grid=(GridView)findViewById(R.id.gridView);
        //grid.setAdapter(adapter);
        /*grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(TGAStore.this, "You Clicked ", Toast.LENGTH_SHORT).show();

            }
        });*/

        ConnectivityManager ConnectionManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true )
        {
            AsyncTaskRunner runner = new AsyncTaskRunner();
            runner.execute();
        }
        else
        {
            Toast.makeText(TGAStore.this, "Network Not Available", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tgastore, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String>  {


        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // display a progress dialog for good user experiance
            progressDialog = new ProgressDialog(TGAStore.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param strings The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected String doInBackground(String... strings) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BaseUrl.baseUrl).addConverterFactory(GsonConverterFactory.create(gson)).build();
            final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
            final String url = "apps_gallery_listing?Type=android&CategoryType=game&PageNo=1";
            final Call<GetDetails> getDetailsCall = requestInterface.getappdetails(url);
            getDetailsCall.enqueue(new Callback<GetDetails>() {
                @Override
                public void onResponse(Call<GetDetails> call, Response<GetDetails> response) {
                    Log.e("Response ::","true");
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {

                        if(response.body().getStatus().equals(status)){
                            for(int i=0;i<response.body().getData().getAppsData().size();i++)
                            {
                                DataClass dataClass = new DataClass();
                                dataClass.setaClass(response.body().getData().getAppsData().get(i).getaClass());
                                dataClass.setAndroidURL(response.body().getData().getAppsData().get(i).getAndroidURL());
                                dataClass.setAppIcon(response.body().getData().getAppsData().get(i).getAppIcon());
                                dataClass.setAppID(response.body().getData().getAppsData().get(i).getAppID());
                                dataClass.setAppName(response.body().getData().getAppsData().get(i).getAppName());

                                AppRatingsClass ratingsClass = new AppRatingsClass();
                                ratingsClass.setAVG_CreativityRating(response.body().getData().getAppsData().get(i).getAppRatings().getAVG_CreativityRating());
                                ratingsClass.setTotalAvgRating(response.body().getData().getAppsData().get(i).getAppRatings().getTotalAvgRating());
                                ratingsClass.setAVG_DesignRating(response.body().getData().getAppsData().get(i).getAppRatings().getAVG_DesignRating());
                                ratingsClass.setAVG_UsabilityRating(response.body().getData().getAppsData().get(i).getAppRatings().getAVG_UsabilityRating());
                                ratingsClass.setTotalVotes(String.valueOf(response.body().getData().getAppsData().get(i).getAppRatings().getTotalVotes()));

                                dataClass.setAppRatings(ratingsClass);

                                dataClass.setAppType(response.body().getData().getAppsData().get(i).getAppType());
                                dataClass.setCategoryID(response.body().getData().getAppsData().get(i).getCategoryID());
                                dataClass.setIOSURL(response.body().getData().getAppsData().get(i).getIOSURL());
                                dataClass.setIsFeatured(response.body().getData().getAppsData().get(i).getIsFeatured());
                                dataClass.setIsPremium(response.body().getData().getAppsData().get(i).getIsPremium());
                                dataClass.setIsSponsor(response.body().getData().getAppsData().get(i).getIsSponsor());
                                dataClass.setTagID(response.body().getData().getAppsData().get(i).getTagID());
                                dataClass.setTotalCount(String.valueOf(response.body().getData().getAppsData().get(i).getTotalCount()));
                                dataClass.setType(response.body().getData().getAppsData().get(i).getType());
                                dataClass.setUploadYear(response.body().getData().getAppsData().get(i).getUploadYear());
                                dataClass.setURL(response.body().getData().getAppsData().get(i).getURL());

                                UserDataClass userDataClass = new UserDataClass();
                                userDataClass.setCompanyName(response.body().getData().getAppsData().get(i).getUserData().getCompanyName());
                                userDataClass.setCountry(response.body().getData().getAppsData().get(i).getUserData().getCountry());
                                userDataClass.setIsFreeUser(response.body().getData().getAppsData().get(i).getUserData().getIsFreeUser());
                                userDataClass.setURL(response.body().getData().getAppsData().get(i).getUserData().getURL());
                                userDataClass.setWebsiteURL(response.body().getData().getAppsData().get(i).getUserData().getWebsiteURL());

                                dataClass.setUserData(userDataClass);

                                dataClass.setUserID(response.body().getData().getAppsData().get(i).getUserID());

                                datalist.add(dataClass);
                                appIcon = response.body().getData().getAppsData().get(i).getAppIcon();
                                appName = response.body().getData().getAppsData().get(i).getAppName();
                                avgRatings = response.body().getData().getAppsData().get(i).getAppRatings().getTotalAvgRating();
                                userVotes = String.valueOf(response.body().getData().getAppsData().get(i).getAppRatings().getTotalVotes());
                                androidUrl = response.body().getData().getAppsData().get(i).getAndroidURL();
                                Tests information=new Tests(appIcon,appName,avgRatings,userVotes,androidUrl);
                                testList.add(information);
                                //CustomGrid adapter = new CustomGrid(TGAStore.this, appIcon , appName , avgRatings , userVotes);
                            }
                            //grid.setAdapter(adapter);
                            mAdapter.notifyDataSetChanged();
                        }

                    } else {
                        Toast.makeText(TGAStore.this, "try again", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<GetDetails> call, Throwable t) {
                    Log.e("Response ::","fail"+t.toString());
                }
            });
            return null;
        }
    }
}

/* try {
                        */