package com.internship.tga.Interface;

import com.internship.tga.Model.GetDetails;
import com.internship.tga.modelnew.SuccessData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiInterface {

        @GET()
        Call<GetDetails> getappdetails(@Url String url);
}
