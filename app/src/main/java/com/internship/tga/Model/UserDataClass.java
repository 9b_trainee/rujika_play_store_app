package com.internship.tga.Model;

import java.net.URL;

public class UserDataClass {

    String CompanyName;
    String URL;
    String Country;
    String WebsiteURL;
    String IsFreeUser;

    public UserDataClass(String companyName, String URL, String country, String websiteURL, String isFreeUser) {
        CompanyName = companyName;
        this.URL = URL;
        Country = country;
        WebsiteURL = websiteURL;
        IsFreeUser = isFreeUser;
    }

    public UserDataClass() {
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String getIsFreeUser() {
        return IsFreeUser;
    }

    public void setIsFreeUser(String isFreeUser) {
        IsFreeUser = isFreeUser;
    }
}
