package com.internship.tga.Model;

public class AppRatingsClass {

    String AVG_DesignRating;
    String AVG_UsabilityRating;
    String AVG_CreativityRating;
    String TotalAvgRating;
    String TotalVotes;

    public AppRatingsClass(String AVG_DesignRating, String AVG_UsabilityRating, String AVG_CreativityRating, String totalAvgRating, String totalVotes) {
        this.AVG_DesignRating = AVG_DesignRating;
        this.AVG_UsabilityRating = AVG_UsabilityRating;
        this.AVG_CreativityRating = AVG_CreativityRating;
        TotalAvgRating = totalAvgRating;
        TotalVotes = totalVotes;
    }

    public AppRatingsClass() {
    }

    public String getAVG_DesignRating() {
        return AVG_DesignRating;
    }

    public void setAVG_DesignRating(String AVG_DesignRating) {
        this.AVG_DesignRating = AVG_DesignRating;
    }

    public String getAVG_UsabilityRating() {
        return AVG_UsabilityRating;
    }

    public void setAVG_UsabilityRating(String AVG_UsabilityRating) {
        this.AVG_UsabilityRating = AVG_UsabilityRating;
    }

    public String getAVG_CreativityRating() {
        return AVG_CreativityRating;
    }

    public void setAVG_CreativityRating(String AVG_CreativityRating) {
        this.AVG_CreativityRating = AVG_CreativityRating;
    }

    public String getTotalAvgRating() {
        return TotalAvgRating;
    }

    public void setTotalAvgRating(String totalAvgRating) {
        TotalAvgRating = totalAvgRating;
    }

    public String getTotalVotes() {
        return TotalVotes;
    }

    public void setTotalVotes(String totalVotes) {
        TotalVotes = totalVotes;
    }
}
