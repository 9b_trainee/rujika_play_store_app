package com.internship.tga.Model;

import java.net.URL;
import java.util.ArrayList;

public class DataClass {

    String AppID;
    String UserID;
    String CategoryID;
    String TagID;
    String AppName;
    String URL;
    String AndroidURL;
    String IOSURL;
    String AppIcon;
    String UploadYear;
    String AppType;
    String Type;
    String IsPremium;
    String IsSponsor;
    String IsFeatured;
    UserDataClass UserData;
    String TotalCount;
    AppRatingsClass AppRatings;
    String aClass;

    public DataClass(String appID, String userID, String categoryID, String tagID, String appName, String URL, String androidURL, String IOSURL, String appIcon, String uploadYear, String appType, String type, String isPremium, String isSponsor, String isFeatured, UserDataClass userData, String totalCount, AppRatingsClass appRatings, String aClass) {
        AppID = appID;
        UserID = userID;
        CategoryID = categoryID;
        TagID = tagID;
        AppName = appName;
        this.URL = URL;
        AndroidURL = androidURL;
        this.IOSURL = IOSURL;
        AppIcon = appIcon;
        UploadYear = uploadYear;
        AppType = appType;
        Type = type;
        IsPremium = isPremium;
        IsSponsor = isSponsor;
        IsFeatured = isFeatured;
        UserData = userData;
        TotalCount = totalCount;
        AppRatings = appRatings;
        this.aClass = aClass;
    }

    public DataClass() {
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String appID) {
        AppID = appID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String tagID) {
        TagID = tagID;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getAndroidURL() {
        return AndroidURL;
    }

    public void setAndroidURL(String androidURL) {
        AndroidURL = androidURL;
    }

    public String getIOSURL() {
        return IOSURL;
    }

    public void setIOSURL(String IOSURL) {
        this.IOSURL = IOSURL;
    }

    public String getAppIcon() {
        return AppIcon;
    }

    public void setAppIcon(String appIcon) {
        AppIcon = appIcon;
    }

    public String getUploadYear() {
        return UploadYear;
    }

    public void setUploadYear(String uploadYear) {
        UploadYear = uploadYear;
    }

    public String getAppType() {
        return AppType;
    }

    public void setAppType(String appType) {
        AppType = appType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getIsPremium() {
        return IsPremium;
    }

    public void setIsPremium(String isPremium) {
        IsPremium = isPremium;
    }

    public String getIsSponsor() {
        return IsSponsor;
    }

    public void setIsSponsor(String isSponsor) {
        IsSponsor = isSponsor;
    }

    public String getIsFeatured() {
        return IsFeatured;
    }

    public void setIsFeatured(String isFeatured) {
        IsFeatured = isFeatured;
    }

    public UserDataClass getUserData() {
        return UserData;
    }

    public void setUserData(UserDataClass userData) {
        UserData = userData;
    }

    public String getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(String totalCount) {
        TotalCount = totalCount;
    }

    public AppRatingsClass getAppRatings() {
        return AppRatings;
    }

    public void setAppRatings(AppRatingsClass appRatings) {
        AppRatings = appRatings;
    }

    public String getaClass() {
        return aClass;
    }

    public void setaClass(String aClass) {
        this.aClass = aClass;
    }
}
