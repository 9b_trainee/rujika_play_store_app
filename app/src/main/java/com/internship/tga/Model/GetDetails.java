package com.internship.tga.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDetails {
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Status")
    private String Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
}
