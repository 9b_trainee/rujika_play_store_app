package com.internship.tga.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppRatings {
    @Expose
    @SerializedName("TotalVotes")
    private int TotalVotes;
    @Expose
    @SerializedName("TotalAvgRating")
    private String TotalAvgRating;
    @Expose
    @SerializedName("AVG(CreativityRating)")
    private String AVG_CreativityRating;//AVG(CreativityRating);
    @Expose
    @SerializedName("AVG(UsabilityRating)")
    private String AVG_UsabilityRating;//AVG(UsabilityRating);
    @Expose
    @SerializedName("AVG(DesignRating)")
    private String AVG_DesignRating;//AVG(DesignRating);

    public int getTotalVotes() {
        return TotalVotes;
    }

    public void setTotalVotes(int TotalVotes) {
        this.TotalVotes = TotalVotes;
    }

    public String getTotalAvgRating() {
        return TotalAvgRating;
    }

    public void setTotalAvgRating(String TotalAvgRating) {
        this.TotalAvgRating = TotalAvgRating;
    }

    public String getAVG_CreativityRating() {
        return AVG_CreativityRating;
    }

    public void setAVG_CreativityRating(String AVG_CreativityRating) {
        this.AVG_CreativityRating = AVG_CreativityRating;
    }

    public String getAVG_UsabilityRating() {
        return AVG_UsabilityRating;
    }

    public void setAVG_UsabilityRating(String AVG_UsabilityRating) {
        this.AVG_UsabilityRating = AVG_UsabilityRating;
    }

    public String getAVG_DesignRating() {
        return AVG_DesignRating;
    }

    public void setAVG_DesignRating(String AVG_DesignRating) {
        this.AVG_DesignRating = AVG_DesignRating;
    }
}
