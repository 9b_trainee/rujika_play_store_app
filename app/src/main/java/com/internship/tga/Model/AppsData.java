package com.internship.tga.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppsData {
    @Expose
    @SerializedName("Class")
    private String aClass;
    @Expose
    @SerializedName("AppRatings")
    private AppRatings AppRatings;
    @Expose
    @SerializedName("TotalCount")
    private int TotalCount;
    @Expose
    @SerializedName("UserData")
    private UserData UserData;
    @Expose
    @SerializedName("IsFeatured")
    private String IsFeatured;
    @Expose
    @SerializedName("IsSponsor")
    private String IsSponsor;
    @Expose
    @SerializedName("IsPremium")
    private String IsPremium;
    @Expose
    @SerializedName("Type")
    private String Type;
    @Expose
    @SerializedName("AppType")
    private String AppType;
    @Expose
    @SerializedName("UploadYear")
    private String UploadYear;
    @Expose
    @SerializedName("AppIcon")
    private String AppIcon;
    @Expose
    @SerializedName("IOSURL")
    private String IOSURL;
    @Expose
    @SerializedName("AndroidURL")
    private String AndroidURL;
    @Expose
    @SerializedName("URL")
    private String URL;
    @Expose
    @SerializedName("AppName")
    private String AppName;
    @Expose
    @SerializedName("TagID")
    private String TagID;
    @Expose
    @SerializedName("CategoryID")
    private String CategoryID;
    @Expose
    @SerializedName("UserID")
    private String UserID;
    @Expose
    @SerializedName("AppID")
    private String AppID;

    public String getaClass() {
        return aClass;
    }

    public void setaClass(String aClass) {
        this.aClass = aClass;
    }

    public AppRatings getAppRatings() {
        return AppRatings;
    }

    public void setAppRatings(AppRatings AppRatings) {
        this.AppRatings = AppRatings;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(int TotalCount) {
        this.TotalCount = TotalCount;
    }

    public UserData getUserData() {
        return UserData;
    }

    public void setUserData(UserData UserData) {
        this.UserData = UserData;
    }

    public String getIsFeatured() {
        return IsFeatured;
    }

    public void setIsFeatured(String IsFeatured) {
        this.IsFeatured = IsFeatured;
    }

    public String getIsSponsor() {
        return IsSponsor;
    }

    public void setIsSponsor(String IsSponsor) {
        this.IsSponsor = IsSponsor;
    }

    public String getIsPremium() {
        return IsPremium;
    }

    public void setIsPremium(String IsPremium) {
        this.IsPremium = IsPremium;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getAppType() {
        return AppType;
    }

    public void setAppType(String AppType) {
        this.AppType = AppType;
    }

    public String getUploadYear() {
        return UploadYear;
    }

    public void setUploadYear(String UploadYear) {
        this.UploadYear = UploadYear;
    }

    public String getAppIcon() {
        return AppIcon;
    }

    public void setAppIcon(String AppIcon) {
        this.AppIcon = AppIcon;
    }

    public String getIOSURL() {
        return IOSURL;
    }

    public void setIOSURL(String IOSURL) {
        this.IOSURL = IOSURL;
    }

    public String getAndroidURL() {
        return AndroidURL;
    }

    public void setAndroidURL(String AndroidURL) {
        this.AndroidURL = AndroidURL;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String AppName) {
        this.AppName = AppName;
    }

    public String getTagID() {
        return TagID;
    }

    public void setTagID(String TagID) {
        this.TagID = TagID;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String CategoryID) {
        this.CategoryID = CategoryID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    public String getAppID() {
        return AppID;
    }

    public void setAppID(String AppID) {
        this.AppID = AppID;
    }
}
