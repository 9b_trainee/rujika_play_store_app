package com.internship.tga;

public class Tests {

    String icon , name , rating , votes , link;

    public Tests(String icon, String name, String rating, String votes, String link) {
        this.icon = icon;
        this.name = name;
        this.rating = rating;
        this.votes = votes;
        this.link = link;
    }

    public Tests() {
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
