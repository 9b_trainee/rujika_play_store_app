package com.internship.tga;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import junit.framework.Test;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private final Context context;
    private List<Tests> testList;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView appIcon;
        public TextView appName;
        public TextView ratings;
        public TextView votes;
        public Button pStore;

        public ViewHolder(View view) {
            super(view);
            appIcon = view.findViewById(R.id.appImage);
            appName = view.findViewById(R.id.appName);
            ratings = view.findViewById(R.id.avgRating);
            votes = view.findViewById(R.id.userVotes);
            pStore = view.findViewById(R.id.playStore);
        }
    }

    public MyAdapter(Context applicationContext, List<Tests> testList) {
        this.context = applicationContext;
        this.testList = testList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tgastore_grid_layout, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Tests test = testList.get(position);
        Picasso.with(context).load(dataClass.AppIcon).placeholder(R.drawable.icon).error(R.drawable.icon).into(holder.img_load, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {

                }
            });
        /*Picasso.with(context)
                .load(test.getIcon().replace("https","http"))
                .into(holder.appIcon);*/
        holder.appName.setText(test.getName());
        holder.ratings.setText(test.getRating());
        holder.votes.setText(test.getVotes()+" users voted");
        holder.pStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(test.getLink()));
                browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                context.startActivity(browserIntent);
                //Intent i = new Intent().setClass(mActivity.getApplication(), TestUserProfileScreenActivity.class);
                // Launch the new activity and add the additional flags to the intent
                //mActivity.getApplication().startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

}